import pandas as pd
import numpy as np

# head -n 2000 demo_pulse.csv > demo_pulse_tm1.csv
# tail -n 2000 demo_pulse.csv > demo_pulse_tm2.csv

df = pd.read_csv("demo_pulse_tm2.csv", sep=",", header=None)
df[1] = "f4f_demo"
df[3] = "f4f_demo"
df[4] = df[4] + 10201
df[5] = df[5] - 20000
df[41] = 3
df[42] = 3
df.to_csv("demo_pulse_tm2.csv", sep=",", header=None, index=None)
