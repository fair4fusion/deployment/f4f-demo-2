from os import getcwd, listdir
from os.path import join, exists
import json

import yaml
import requests


# ********************************** Login & Init *************************************************
def read_credentials():
    """
    Function that reads a yaml file containing the user's credentials
    Returns
        | dict: a dictionary with the contents of the yaml file
    """
    cred_file = join(getcwd(), "credentials.yaml")
    example_cred_file = join(getcwd(), "example_credentials.yaml")
    cred_filename = cred_file if exists(cred_file) else example_cred_file
    with open(cred_filename, "r") as f:
        return yaml.safe_load(f)


def login(hostname, username, password, requested_issuer):
    """
    User authentication in the F4F demonstrator.

    Args
        | hostname (str): the base url of the F4F demonstrator (IP and port or domain)
        | username (str): the username
        | password (str): the password of the user
        | requested_issuer (str): provide this parameter if you are using 3rd party identity provider

    Returns
        | dict: a dictionary with the session token, the refresh token and the expiration time
    """
    data = {
        "username": username,
        "password": password,
        "requested_issuer": requested_issuer
    }
    print(data)
    headers = {"Content-Type": "application/json"}
    r = requests.post(hostname + '/api/accounts/login/', data=json.dumps(data), headers=headers)
    if r.status_code == 200:
        response = json.loads(json.loads(r.text))
        return {"access_token": response["access_token"], "refresh_token": response["refresh_token"],
                "expires_in": response["expires_in"]}
    else:
        print("Could not authenticate user!")


# ********************** Pulses Data ************************************************
def create_pulses(base_url, token):
    """
    Data ingestion client function. Provides a json with the machines and their data.

    Args
        | base_url (str): the base URL to the F4F demonstrator
        | token (str): user's session token
    """
    data_folder = join(getcwd(), "data")
    files = []
    machines = listdir(data_folder)
    
    if machines:
        machines_dict = {}
        for machine in machines:
            print(machine)
            csvs = listdir(join(data_folder, machine))
            csvs_dict = {}
            for csv in csvs:
                print(csv)
                with open(join(data_folder, machine, csv), "r") as f:
                    csvs_dict[csv] = f.read()
            machines_dict[machine] = csvs_dict
        files.append(machines_dict)
        data = {"files": files, "access_token": token}
        url = base_url + "/api/pulses-full/"
        response = requests.post(url=url, json=data)
        print(response.status_code, response.text)
    else:
        print("No machine folders are found in the data folder")


def populate_db():
    """
    Function to ingest data in the DB. Uses the read_credentials, login and create_pulses functions
    """
    credentials = read_credentials()
    print("Done reading credentials")
    print(credentials)
    token = login(credentials["base_url"], credentials["username"], credentials["password"], credentials["issuer"])
    print(token)
    create_pulses(credentials["base_url"], token["access_token"])


if __name__ == '__main__':
    populate_db()