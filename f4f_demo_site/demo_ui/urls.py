from django.conf.urls import url, include
from django.urls import path
from django.contrib import admin

from demo_ui.views import *

urlpatterns = [
    # url(r'^register/$', register, name='register'),
    # url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, {'next_page': 'login'}, name='logout'),
    url(r'^pulses/$', get_pulses, name="pulses-list"),
    url(r'^machines/$', get_machines, name="machines-list"),
    url(r'^pids/$', get_pids, name="pids-list"),
    url(r'^users/$', get_users, name="users-list"),
    url(r'^experiments/$', get_experiments, name="experiments-list"),
    path("<str:experiment>/show", view_experiment, name="experiment-details"),
    path("<str:experiment>/<int:query_shot>/results", view_experiment_results, name="experiment-results"),
    path("<str:username>/profile", user_profile, name="profile"),
    path("<int:pk>/overview", view_pulse, name="pulse-details"),
    url(r'^update-session/$', update_session, name="session-update"),
    path("", get_pulses, name="pulses-list"),
]
