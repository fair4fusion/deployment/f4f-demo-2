import json
import logging
import re
from math import ceil
from time import time

import requests
from django.shortcuts import render, redirect
from django.http import HttpResponseNotAllowed, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from demo_ui import utils
from demo_ui.forms import *
from f4f_demo_site.settings import API_BASE_URL, properties
import jwt

logger = logging.getLogger(__name__)


def my_login_required(func):
    """
    Decorator to redirect a view. If Django session contains a token and a username, the user is signed in and the
    view redirects to the selected UI view, otherwise he/she is redirected to the login view

    Args
        | func (function): a UI function that loads a view

    Returns
        | view: the view that will be loaded
    """
    def check_login(request):
        decodedPayload = jwt.decode(
            request.session['oidc_access_token'], options={"verify_signature": False})
        if "fair4fusion/deployment" in decodedPayload['f4f-groups']:
            return func(request)
        else:
            return HttpResponse("Not part of the f4f group!")
    return login_required(check_login)


def check_group(token):
    decodedPayload = jwt.decode(
        token, options={"verify_signature": False})
    if "fair4fusion/pipelines" in decodedPayload['f4f-groups']:
        return True
    else:
        return False


# if check_group(request.session['oidc_access_token']) == False:
#     return HttpResponse("Not part of the f4f group!")
# ****************************************** decorators *************************************************
# def login_required(func):
#     """
#     Decorator to redirect a view. If Django session contains a token and a username, the user is signed in and the
#     view redirects to the selected UI view, otherwise he/she is redirected to the login view

#     Args
#         | func (function): a UI function that loads a view

#     Returns
#         | view: the view that will be loaded
#     """

#     def check_login(request, *args, **kwargs):
#         if "username" in request.session.keys() and "access_token" in request.session.keys():
#             return func(request, *args, **kwargs)
#         else:
#             return redirect("login", permanent=True)

#     return check_login


# **************************************** UI views ********************************************************
# def register(request):
#     # TODO is registration allowed??
#     return redirect("login", permanent=True)


# def login(request):
#     """
#     UI view for login. The user should provide a valid username and password in order to access the application.
#     The demonstrator uses keycloak as backend through the dare-login component.

#     When user presses the login button, the function receives the username and password parameters and performs an
#     API call to the /api/accounts/login/ endpoint in order to get a session token.
#     """
#     if "username" in request.POST.keys():
#         username = request.POST["username"]
#         password = request.POST["password"]
#         # username = "test"
#         # password = "test"
#         url = API_BASE_URL + "/api/accounts/login/"
#         try:
#             response = requests.post(url, data={"username": username, "password": password, "requested_issuer": "test"})
#             # if def check_group(token):
    # decodedPayload = jwt.decode(token, None, None)
    # if "f4f-test-group" in decodedPayload['f4f-groups']:
    #     return True
    # return False
#                 # response = json.loads(response.json())
#                 # context = {"login_form": LoginForm(), "DASHBOARD_VERSION": utils.version_info(),
#                            # "base_url": API_BASE_URL, "fail": response["error_description"]}
#                 # return render(request, "users/login.html", context=context)
#         except(ConnectionError, ConnectionRefusedError, Exception, BaseException):
#             context = {"login_form": LoginForm(), "DASHBOARD_VERSION": utils.version_info(),
#                        "base_url": API_BASE_URL, "fail": "Connection error: Can't login right now!"}
#             return render(request, "users/login.html", context=context)
#     else:
#         utils.cleanup_session(request)
#         context = {"login_form": LoginForm(), "DASHBOARD_VERSION": utils.version_info(), "base_url": API_BASE_URL}
#         return render(request, "users/login.html", context=context)


@login_required
def user_profile(request, username):
    """
    UI view to logged in user's profile. The page shows the user details, allows the editing of the profile (only if
    the profile showed matches the logged in user) and shows some recent annotations of the user

    Args
        | request (WSGIRequest): the request to the page
        | username (str): the user's profile to show
    """
    if check_group(request.session['oidc_access_token']) == False:
        return HttpResponse("Not part of the f4f group!")
    session_username = request.user.username  # request.session["username"]
    session_token = request.session['oidc_access_token']
    user_response = requests.get(API_BASE_URL + "/api/users/byusername",
                                 params={"username": session_username, "access_token": session_token})
    user = user_response.json()
    if(user_response.status_code == 200):
        user["date_joined"] = datetime.strptime(user["date_joined"].split(".")[
                                                0], '%Y-%m-%dT%H:%M:%S').date()
        annotations = requests.get(API_BASE_URL + "/api/annotations-paged/",
                                   params={"username": user["username"], "access_token": session_token}).json()
        annotations = annotations["results"]
        annotations = annotations[:10]
        context = {"user": user, "DASHBOARD_VERSION": utils.version_info(), "username": session_username,
                   "annotations": annotations, "access_token": session_token}
        if username == session_username:
            user_id = request.user.id
            profile_form = ProfileForm()
            context["user_id"] = user_id
            context["profile_form"] = profile_form
        return render(request, "users/profile.html", context=context)

    else:
        return get_users(request)


def logout(request, next_page):
    """
    Function to logout the user if selected from the UI. It clears the session and redirects the user to the login
    page.

    Args
        | request: http request to load a view
        | next_page (str): the page to be redirected after the clearning of the session

    Returns
        | view: redirects the user to the login page
    """
    try:
        utils.cleanup_session(request)
        return redirect(next_page, permanent=True)
    except (BaseException, Exception) as e:
        logger.error(e)
        return redirect(next_page, permanent=True)


@login_required
def get_pulses(request):
    """
    UI view showing the list of pulses. The list is updated based on the selected page, as well as based on the
    filled fields of the search form. The function checks if the user has search for specific machine, owner, dates
    or annotations and updates the results accordingly.
    The function provides the search form, the results and the paging urls to the html template

    Args
        | request: http request to load the list of pulses

    Returns
        | view: loads the pulses list UI
    """
    if check_group(request.session['oidc_access_token']) == False:
        return HttpResponse("Not part of the f4f group!")
    if "search_for" in request.GET.keys():
        # update list after searching
        logger.debug("Request contains search params")
        search_txt = request.GET["search_for"]
        start_date = request.GET["start_date"]
        end_date = request.GET["end_date"]
        machine = request.GET["machine"]
        owner = request.GET["owner"]
        page = request.GET["page"] if "page" in request.GET.keys() else None
        data = utils.get_search_data(search_txt=search_txt, start_date=start_date, end_date=end_date, machine=machine,
                                     owner=owner, token=request.session['oidc_access_token'], page=page)
    elif "pid_id" in request.GET.keys():
        pid_id = request.GET["pid_id"]
        page = request.GET["page"] if "page" in request.GET.keys() else None
        data = utils.get_pulses_in_pid(
            pid_id=pid_id, token=request.session['oidc_access_token'], page=page)
    elif "shot_number" in request.GET.keys():
        shot_number = request.GET["shot_number"]
        page = request.GET["page"] if "page" in request.GET.keys() else None
        data = utils.get_pulses_by_shot(
            shot_number=shot_number, token=request.session['oidc_access_token'], page=page)
    else:
        logger.debug(
            "Request does not contain any search params. View will show all pulses")
        # check if page parameter is in the request to change the list results
        if "page" in request.GET.keys() and request.GET["page"]:
            page = request.GET["page"]
            if "api/pulses" in request.GET["page"]:
                page_parts = request.GET["page"].split("?")
                page = page_parts[1].split("=")[1]
            data = requests.get(API_BASE_URL + "/api/pulses",
                                params={"page": page, "access_token": request.session['oidc_access_token']}).json()
        else:
            logger.error(request.session['oidc_access_token'])
            logger.error(request.session.keys())
            logger.error(request.user.username)
            logger.error(request.user.id)
            logger.error(request.user.is_superuser)
            data = requests.get(API_BASE_URL + "/api/pulses",
                                params={"access_token": request.session['oidc_access_token']}).json()
    context = {"username": request.user.username, "is_admin": request.user.is_superuser,
               "access_token": request.session["oidc_access_token"], "max_download": properties["pulse_max_download"]}
    # change the link to request next page, it should call the UI view first
    if "previous" in data.keys() and data["previous"]:
        data["previous"] = data["previous"].replace("/api/", "/ui/")
        context["first_page"] = re.sub(
            "page=[0-9]", "page=1", data["previous"])
    if "next" in data.keys() and data["next"]:
        data["next"] = data["next"].replace("/api/", "/ui/")
        last_page = ceil(int(data["lastPage"]) / data["countItemsOnPage"])
        context["last_page"] = re.sub(
            "page=[0-9]", "page=" + str(last_page), data["next"])
    pulses = data["results"]
    machines = utils.collect_machine_names(
        request.session["oidc_access_token"])
    owners = utils.collect_usernames(request.session["oidc_access_token"])
    if pulses and type(pulses) == list:
        for pulse in pulses:
            if pulse["annotations"]:
                annotations = pulse["annotations"]
                pulse["cannots_list"] = utils.transform_annotations(
                    annotations, pulse_format="list")
                pulse["cannots"] = utils.transform_annotations(
                    annotations) + "..."
        context['object_list'] = pulses
    if request.GET:
        parameters = request.GET.copy()
        context["filter_form"] = FilterForm(
            label_suffix="", machines=machines, owners=owners, parameters=parameters)
    else:
        context["filter_form"] = FilterForm(
            label_suffix="", machines=machines, owners=owners)
    context["DASHBOARD_VERSION"] = utils.version_info()
    context["previous"] = data["previous"]
    context["next"] = data["next"]
    return render(request, 'pulses/pulse_list.html', context)

@login_required
def get_machines(request):
    """
    UI view to list the available machines in the DB. The view allows the filtering of the machines and shows the
    results in pages.
    """
    if check_group(request.session['oidc_access_token']) == False:
        return HttpResponse("Not part of the f4f group!")
    if "machine" in request.GET.keys():
        # update list after searching
        logger.debug("Request contains search params")
        machine = request.GET["machine"]
        page = request.GET["page"] if "page" in request.GET.keys() else None
        data = utils.get_machine_data(
            token=request.session["oidc_access_token"], machine=machine, page=page)
    else:
        logger.debug(
            "Request does not contain any search params. View will show all machines")
        # check if page parameter is in the request to change the list results
        if "page" in request.GET.keys() and request.GET["page"]:
            page = request.GET["page"]
            if "api/experiments" in request.GET["page"]:
                page_parts = request.GET["page"].split("?")
                page = page_parts[1].split("=")[1]
            data = requests.get(API_BASE_URL + "/api/experiments-paged",
                                params={"page": page, "access_token": request.session["oidc_access_token"]}).json()
        else:
            data = requests.get(API_BASE_URL + "/api/experiments-paged",
                                params={"access_token": request.session["oidc_access_token"]}).json()
    context = {"username": request.user.username, "is_admin": request.user.is_superuser,
               "machine_form": MachineForm(), "access_token": request.session["oidc_access_token"]}

    # change the link to request next page, it should call the UI view first
    if "previous" in data.keys() and data["previous"]:
        data["previous"] = data["previous"].replace("/api/", "/ui/")
        context["first_page"] = re.sub(
            "page=[0-9]", "page=1", data["previous"])
    if "next" in data.keys() and data["next"]:
        data["next"] = data["next"].replace("/api/", "/ui/")
        last_page = ceil(int(data["lastPage"]) / data["countItemsOnPage"])
        context["last_page"] = re.sub(
            "page=[0-9]", "page=" + str(last_page), data["next"])

    machine_list = data["results"]
    if machine_list and type(machine_list) == list:
        context["machine_list"] = machine_list
        count = 0
        for machine in machine_list:
            count = count + 1
            machine["index"] = count
    machines = utils.collect_machine_names(
        request.session["oidc_access_token"])
    if request.GET:
        parameters = request.GET.copy()
        context["machine_search_form"] = MachineSearchForm(
            label_suffix="", machines=machines, parameters=parameters)
    else:
        context["machine_search_form"] = MachineSearchForm(
            label_suffix="", machines=machines)
    context["DASHBOARD_VERSION"] = utils.version_info()
    context["previous"] = data["previous"]
    context["next"] = data["next"]
    return render(request, 'machines/machine_list.html', context)


@login_required
def get_experiments(request):
    """
    UI view to list the available experiments in the DB. The view allows the filtering of the machines and shows the
    results in pages.
    """
    if check_group(request.session['oidc_access_token']) == False:
        return HttpResponse("Not part of the f4f group!")
  
    # data = requests.get(API_BASE_URL + "/api/pids-paged",
                                # params={"access_token": token}).json()

    url = "http://rest-operator.fair4fusion.iit.demokritos.gr/show"
    response = requests.get(url,params = {"arg":"."},headers = {"Authorization": "Bearer " + request.session['oidc_access_token']})

    files = response.text.split('\n')
    experiments = [file for file in files if file.startswith('experiment-')]

    data = []
    for exp in experiments:
        temp_data = {}
        temp_data["name"] = exp

        url = "http://rest-operator.fair4fusion.iit.demokritos.gr/show"
        response = requests.get(url,params = {"arg":"./" + exp + "/out/"},headers = {"Authorization": "Bearer " + request.session['oidc_access_token']})
        shots = response.text.split('\n')
        query_shots = [shot for shot in shots if shot != '']
        logger.error(query_shots)
        
        temp_data["query_shots"] = query_shots
        data.append(temp_data)
        logger.error(data)
   
    # data = [{'name': 'experiment-Spjrr9Brd8', 'query_shots': ['37560', '38109', '38121', '38185']}]

    context = {"DASHBOARD_VERSION": utils.version_info(),
                "username": request.user.username, 
                "is_admin": request.user.is_superuser, "access_token": request.session["oidc_access_token"],
                "out_dir": "analytics"}

    context["experiments_list"] = data
    return render(request, 'experiments/experiments_list.html', context)



@login_required
def view_experiment(request,experiment):
    """
    UI view to list the available experiments in the DB. The view allows the filtering of the machines and shows the
    results in pages.
    """
    if check_group(request.session['oidc_access_token']) == False:
        return HttpResponse("Not part of the f4f group!")
  
    # data = requests.get(API_BASE_URL + "/api/pids-paged",
                                # params={"access_token": token}).json()

    url = "http://rest-operator.fair4fusion.iit.demokritos.gr/show"
    response = requests.get(url,params = {"arg":"./" + experiment + "/out/"},headers = {"Authorization": "Bearer " + request.session['oidc_access_token']})
    shots = response.text.split('\n')
    query_shots = [shot for shot in shots if shot != '']

    logger.error(experiment)
    context = {"DASHBOARD_VERSION": utils.version_info(),
                    "username": request.user.username, 
                    "is_admin": request.user.is_superuser, "access_token": request.session["oidc_access_token"],
                    "out_dir": "analytics"}

    context["query_shots"] = query_shots
    context["experiment"] = experiment
    
    return render(request, 'experiments/experiments_queries_list.html', context)


@login_required
def view_experiment_results(request,experiment,query_shot):
    """
    UI view to list the available experiments in the DB. The view allows the filtering of the machines and shows the
    results in pages.
    """
    if check_group(request.session['oidc_access_token']) == False:
        return HttpResponse("Not part of the f4f group!")
  
    request_data = {"shot": query_shot,
                    "access_token": request.session["oidc_access_token"]}
    # get pulse details
    response = requests.get(
        API_BASE_URL + "/api/pulses-full/shot_details", params=request_data)
    data = response.json() 

    url = "http://rest-operator.fair4fusion.iit.demokritos.gr/fetch"
    response = requests.get(url,params = {"arg":"./" + experiment + "/out/" + str(query_shot) + "/output_smallest.txt"},headers = {"Authorization": "Bearer " + request.session['oidc_access_token']})
    shots = response.text.split('\n')
    similar_shots = [int(shot) for shot in shots if shot != '']

    similar_ids = []
    sim_data = [data]
    for pulse in similar_shots:
        logger.error(pulse)
        temp_request_data = {"shot": pulse,
                        "access_token": request.session["oidc_access_token"]}
        temp_response = requests.get(
            API_BASE_URL + "/api/pulses-full/shot_details", params=temp_request_data)
        temp_resp_data = temp_response.json()
        sim_data.append(temp_resp_data)
        similar_ids.append(temp_resp_data["id"])

    ct = experiment.split("-")[-1] + " value"
    comparisonType = ct.capitalize()
    
    logger.error(comparisonType)

    if(response.status_code == 200):

        # get annotations
        annotations = sorted(data["annotations"],
                             key=lambda k: k["time"], reverse=True)
        annotations = utils.transform_annotations(
            annotations, pulse_format="detail")
        if annotations:
            data["cannots"] = annotations


    
        # get associated PID(s)
        pids = data["pids"]

        # create plots
        logger.debug("Collecting plots if they exist")
        plot_info = properties["plot"]
        fields_to_plot = request.GET["fields_to_plot"] if "fields_to_plot" in request.GET.keys() else plot_info[
            "fields_to_plot"]
        multi_figs = utils.visualize_multiple_pulses(pulses=sim_data,fields_to_plot=fields_to_plot, width=plot_info["width"], height=plot_info["height"],multi=True)
        figs = utils.visualize_parameters(pulse=data, fields_to_plot=fields_to_plot, width=plot_info["width"], height=plot_info["height"])
        figs_table = utils.visualize_parameters(pulse=data, fields_to_plot=fields_to_plot, width=plot_info["width"], height=plot_info["height"])
        analytics_properties = properties["analytics"]
        context = {"pulse": data, "DASHBOARD_VERSION": utils.version_info(), "plot_width": plot_info["width"],
                   "plot_height": plot_info["height"], "annotation_form": AnnotationForm(), "pk": data["id"], "plot_images": figs,
                   "username": request.user.username, "variables": plot_info["fields_to_plot"],
                   "is_admin": request.user.is_superuser, "access_token": request.session["oidc_access_token"],
                   "out_dir": "analytics", "analytics_properties": analytics_properties, "plot_images_table": figs_table}

        context["comparisonType"] = comparisonType
        context["multi_plots"] = multi_figs
        context["similar"] = similar_shots
        session_data_key = "shot_{}".format(data["shot"])
        if session_data_key in request.session:
            context["kind_retrieve"] = "stored"
            context["analytics"] = request.session[session_data_key]
        else:
            # utils.calc_analytics(data, fields_to_plot, request.session["access_token"])
            run_dir = ""
            context["kind_retrieve"] = "job"
            context["analytics"] = {"job": "running"}
            context["run_dir"] = run_dir
            context["filename"] = analytics_properties["filename"]
        if pids:
            context["pids"] = pids
        return render(request, 'pulses/pulse_result_detail.html', context)
    else:
        return get_pulses(request)
    


@login_required
def get_pids(request):
    """
    PID list view which shows the available PIDs. They can be filter by description and page url or from the dropdown
    menu with the issuer.
    """
    if check_group(request.session['oidc_access_token']) == False:
        return HttpResponse("Not part of the f4f group!")
    token = request.session["oidc_access_token"]
    if "pid_number" in request.GET.keys():
        # update list after searching
        logger.debug("Request contains search params")
        pid_number = request.GET["pid_number"]
        page = request.GET["page"] if "page" in request.GET.keys() else None
        data = utils.get_pid_data(
            token=token, pid_number=pid_number, page=page)
    elif "search_for" in request.GET.keys():
        search_txt = request.GET["search_for"]
        issuer = request.GET["issuer"]
        page = request.GET["page"] if "page" in request.GET.keys() else None
        data = utils.get_pid_data(
            token=token, search_txt=search_txt, issuer=issuer, page=page)
    else:
        logger.debug(
            "Request does not contain any search params. View will show all PIDs")
        # check if page parameter is in the request to change the list results
        if "page" in request.GET.keys() and request.GET["page"]:
            page = request.GET["page"]
            if "api/pids" in request.GET["page"]:
                page_parts = request.GET["page"].split("?")
                page = page_parts[1].split("=")[1]
            data = requests.get(API_BASE_URL + "/api/pids-paged",
                                params={"page": page, "access_token": token}).json()
        else:
            data = requests.get(API_BASE_URL + "/api/pids-paged",
                                params={"access_token": token}).json()
    context = {"username": request.user.username, "is_admin": request.user.is_superuser, "pid_form": PidForm(),
               "access_token": token}
    # change the link to request next page, it should call the UI view first
    if "previous" in data.keys() and data["previous"]:
        data["previous"] = data["previous"].replace("/api/", "/ui/")
        context["first_page"] = re.sub(
            "page=[0-9]", "page=1", data["previous"])
    if "next" in data.keys() and data["next"]:
        data["next"] = data["next"].replace("/api/", "/ui/")
        last_page = ceil(int(data["lastPage"]) / data["countItemsOnPage"])
        context["last_page"] = re.sub(
            "page=[0-9]", "page=" + str(last_page), data["next"])
    pid_list = data["results"]
    if pid_list and type(pid_list) == list:
        context["pid_list"] = pid_list
        for pid in pid_list:
            pulses = utils.get_pulses_in_pid(
                pid["pid_number"], token=token)["results"]
            if pulses:
                if len(pulses) > 10:
                    pulses = pulses[:10]
                pulses_str = None
                for pulse in pulses:
                    shot = str(pulse["shot"])
                    pulses_str = pulses_str + ", " + shot if pulses_str else shot
                pid["pulses"] = pulses_str
            else:
                pid["pulses"] = "----"
    issuers = utils.collect_pid_issuers(token=token)
    if not issuers:
        issuers = []
    if request.GET:
        parameters = request.GET.copy()
        context["pid_search_form"] = PidSearchForm(
            label_suffix="", issuers=issuers, parameters=parameters)
    else:
        context["pid_search_form"] = PidSearchForm(
            label_suffix="", issuers=issuers)
    context["DASHBOARD_VERSION"] = utils.version_info()
    context["previous"] = data["previous"]
    context["next"] = data["next"]
    return render(request, 'pids/pid_list.html', context)


@login_required
@csrf_exempt
def get_simillar(request):
    """
    PID list view which shows the available PIDs. They can be filter by description and page url or from the dropdown
    menu with the issuer.
    """
    if check_group(request.session['oidc_access_token']) == False:
        return HttpResponse("Not part of the f4f group!")
    token = request.session["oidc_access_token"]
    if "pid_number" in request.GET.keys():
        # update list after searching
        logger.debug("Request contains search params")
        pid_number = request.GET["pid_number"]
        page = request.GET["page"] if "page" in request.GET.keys() else None
        data = utils.get_pid_data(
            token=token, pid_number=pid_number, page=page)
    elif "search_for" in request.GET.keys():
        search_txt = request.GET["search_for"]
        issuer = request.GET["issuer"]
        page = request.GET["page"] if "page" in request.GET.keys() else None
        data = utils.get_pid_data(
            token=token, search_txt=search_txt, issuer=issuer, page=page)
    else:
        logger.debug(
            "Request does not contain any search params. View will show all PIDs")
        # check if page parameter is in the request to change the list results
        if "page" in request.GET.keys() and request.GET["page"]:
            page = request.GET["page"]
            if "api/pids" in request.GET["page"]:
                page_parts = request.GET["page"].split("?")
                page = page_parts[1].split("=")[1]
            data = requests.get(API_BASE_URL + "/api/pids-paged",
                                params={"page": page, "access_token": token}).json()
        else:
            data = requests.get(API_BASE_URL + "/api/pids-paged",
                                params={"access_token": token}).json()
    context = {"username": request.user.username, "is_admin": request.user.is_superuser, "pid_form": PidForm(),
               "access_token": token}
    # change the link to request next page, it should call the UI view first
    if "previous" in data.keys() and data["previous"]:
        data["previous"] = data["previous"].replace("/api/", "/ui/")
        context["first_page"] = re.sub(
            "page=[0-9]", "page=1", data["previous"])
    if "next" in data.keys() and data["next"]:
        data["next"] = data["next"].replace("/api/", "/ui/")
        last_page = ceil(int(data["lastPage"]) / data["countItemsOnPage"])
        context["last_page"] = re.sub(
            "page=[0-9]", "page=" + str(last_page), data["next"])
    pid_list = data["results"]
    if pid_list and type(pid_list) == list:
        context["pid_list"] = pid_list
        for pid in pid_list:
            pulses = utils.get_pulses_in_pid(
                pid["pid_number"], token=token)["results"]
            if pulses:
                if len(pulses) > 10:
                    pulses = pulses[:10]
                pulses_str = None
                for pulse in pulses:
                    shot = str(pulse["shot"])
                    pulses_str = pulses_str + ", " + shot if pulses_str else shot
                pid["pulses"] = pulses_str
            else:
                pid["pulses"] = "----"
    issuers = utils.collect_pid_issuers(token=token)
    if not issuers:
        issuers = []
    if request.GET:
        parameters = request.GET.copy()
        context["pid_search_form"] = PidSearchForm(
            label_suffix="", issuers=issuers, parameters=parameters)
    else:
        context["pid_search_form"] = PidSearchForm(
            label_suffix="", issuers=issuers)
    context["DASHBOARD_VERSION"] = utils.version_info()
    context["previous"] = data["previous"]
    context["next"] = data["next"]
    return render(request, 'pids/pid_list.html', context)


@login_required
def get_users(request):
    """
    List view showing the available application users 
   """
    if check_group(request.session['oidc_access_token']) == False:
        return HttpResponse("Not part of the f4f group!")
    token = request.session["oidc_access_token"]
    if "usernames" in request.GET.keys():
        # update list after searching
        logger.debug("Request contains search params")
        username = request.GET["usernames"]
        first_name = request.GET["first_names"]
        last_name = request.GET["last_names"]
        email = request.GET["emails"]
        page = request.GET["page"] if "page" in request.GET.keys() else None
        data = utils.get_user_data(token=token, username=username, first_name=first_name, last_name=last_name,
                                   email=email, page=page)
    else:
        logger.debug(
            "Request does not contain any search params. View will show all PIDs")
        # check if page parameter is in the request to change the list results
        if "page" in request.GET.keys() and request.GET["page"]:
            page = request.GET["page"]
            if "api/pids" in request.GET["page"]:
                page_parts = request.GET["page"].split("?")
                page = page_parts[1].split("=")[1]
            data = requests.get(API_BASE_URL + "/api/users-paged",
                                params={"page": page, "access_token": token}).json()
        else:
            data = requests.get(API_BASE_URL + "/api/users-paged",
                                params={"access_token": token}).json()
    context = {"username": request.user.username, "is_admin": request.user.is_superuser,
               "user_form": RegisterForm(), "access_token": request.session["oidc_access_token"]}
    # change the link to request next page, it should call the UI view first
    if "previous" in data.keys() and data["previous"]:
        data["previous"] = data["previous"].replace("/api/", "/ui/")
        context["first_page"] = re.sub(
            "page=[0-9]", "page=1", data["previous"])
    if "next" in data.keys() and data["next"]:
        data["next"] = data["next"].replace("/api/", "/ui/")
        last_page = ceil(int(data["lastPage"]) / data["countItemsOnPage"])
        context["last_page"] = re.sub(
            "page=[0-9]", "page=" + str(last_page), data["next"])
    user_list = data["results"]
    if user_list and type(user_list) == list:
        context["user_list"] = user_list
        count = 0
        for user in user_list:
            count += 1
            user["is_superuser"] = "Admin" if user["is_superuser"] else "User"
            user["index"] = count

    user_data = utils.collect_user_data(token=token)
    if request.GET:
        parameters = request.GET.copy()
        context["user_search_form"] = UserSearchForm(label_suffix="", usernames=user_data["usernames"],
                                                     first_names=user_data["first_names"],
                                                     last_names=user_data["last_names"], emails=user_data["emails"],
                                                     parameters=parameters)
    else:
        context["user_search_form"] = UserSearchForm(label_suffix="", usernames=user_data["usernames"],
                                                     first_names=user_data["first_names"],
                                                     last_names=user_data["last_names"], emails=user_data["emails"])
    context["DASHBOARD_VERSION"] = utils.version_info()
    context["previous"] = data["previous"]
    context["next"] = data["next"]
    return render(request, 'users/user_list.html', context)

@login_required
def upload_experiment(request): 
    context = {}
    return render(request, 'users/create.html', context)



@login_required
def view_pulse(request, pk):
    """
    Function that loads the detailed view. It makes use of the RESTful API to retrieve a specific pulse and updates
    the view with the necessary information and plots.

    Args
        | request: http request to load pulse details
        | pk (int): primary key of the selected pulse

    Returns
        | view: the view with pulse details
    """
    if check_group(request.session['oidc_access_token']) == False:
        return HttpResponse("Not part of the f4f group!")
    request_data = {"pulse_id": pk,
                    "access_token": request.session["oidc_access_token"]}
    # get pulse details
    response = requests.get(
        API_BASE_URL + "/api/pulses-full/details", params=request_data)
    data = response.json()


    if(response.status_code == 200):
        logger.debug("Transform annotations for pulse with id {}".format(pk))

        # get annotations
        annotations = sorted(data["annotations"],
                             key=lambda k: k["time"], reverse=True)
        annotations = utils.transform_annotations(
            annotations, pulse_format="detail")
        if annotations:
            data["cannots"] = annotations


        # get associated PID(s)
        pids = data["pids"]

        # create plots
        logger.debug("Collecting plots if they exist")
        plot_info = properties["plot"]
        fields_to_plot = request.GET["fields_to_plot"] if "fields_to_plot" in request.GET.keys() else plot_info[
            "fields_to_plot"]
        figs = utils.visualize_parameters(pulse=data, fields_to_plot=fields_to_plot, width=plot_info["width"], height=plot_info["height"])
        figs_table = utils.visualize_parameters(pulse=data, fields_to_plot=fields_to_plot, width=plot_info["width"], height=plot_info["height"])
        analytics_properties = properties["analytics"]
        context = {"pulse": data, "DASHBOARD_VERSION": utils.version_info(), "plot_width": plot_info["width"],
                   "plot_height": plot_info["height"], "annotation_form": AnnotationForm(), "pk": pk, "plot_images": figs,
                   "username": request.user.username, "variables": plot_info["fields_to_plot"],
                   "is_admin": request.user.is_superuser, "access_token": request.session["oidc_access_token"],
                   "out_dir": "analytics", "analytics_properties": analytics_properties, "plot_images_table": figs_table}

        session_data_key = "shot_{}".format(data["shot"])
        if session_data_key in request.session:
            context["kind_retrieve"] = "stored"
            context["analytics"] = request.session[session_data_key]
        else:
            # utils.calc_analytics(data, fields_to_plot, request.session["access_token"])
            run_dir = ""
            context["kind_retrieve"] = "job"
            context["analytics"] = {"job": "running"}
            context["run_dir"] = run_dir
            context["filename"] = analytics_properties["filename"]
        if pids:
            context["pids"] = pids
        return render(request, 'pulses/pulse_detail.html', context)
    else:
        return get_pulses(request)


@csrf_exempt
def update_session(request):
    if not request.is_ajax() or not request.method == "POST":
        return HttpResponseNotAllowed(['POST'])
    data = json.loads(list(dict(request.POST).keys())[0])
    key = "shot_{}".format((data["shot"]))
    request.session[key] = data
    return HttpResponse('ok')
