from django.apps import AppConfig


class DemoUiConfig(AppConfig):
    name = 'demo_ui'
