from datetime import datetime
from django.core.files.uploadedfile import UploadedFile

from django.forms import (
    Form,
    DateField,
    CharField,
    ChoiceField,
    DateInput,
    TextInput,
    Textarea,
    PasswordInput,
    Select
)

YEAR_RANGE = range(1983, datetime.now().year + 1)


class DateInput(DateInput):
    input_type = "date"


class FilterForm(Form):
    """
    Class representing the Search Form in the UI. The available search fields are the following:

        | * Search for: free text field that searches all the available annotations of the pulses
        | * Start and End dates: date fields that apply a date range in the results based on their creation date
        | * Machine: drop down field containing the available machine names in the DB
        | * Owner: drop down field containing the available usernames in the DB
    """
    search_for = CharField(
        required=False,
        widget=TextInput(attrs={"placeholder": "e.g. restart, 87470, peter", "class": "form-control"}),
    )
    start_date = DateField(widget=DateInput(attrs={"class": "form-control"}), label="Start date", required=False)
    end_date = DateField(widget=DateInput(attrs={"class": "form-control"}), label="End date", required=False)
    machine = ChoiceField(choices=(), initial=('', '---Please select a machine---'), label="Machine", required=False,
                          widget=Select(attrs={"class": "custom-select mr-sm-2"}))
    owner = ChoiceField(choices=(), initial=('', '---Please select an owner---'), label="Owner Username",
                        required=False, widget=Select(attrs={"class": "custom-select mr-sm-2"}))

    def __init__(self, label_suffix, machines, owners, parameters=None):
        """
        Search form constructor. Creates the form and dynamically updates the drop down lists using
        all the available DB entries.
        """
        super(FilterForm, self).__init__(parameters, label_suffix)
        new_machines = [('', '---Select a machine---')]
        for machine in machines:
            new_machines.append((machine, machine))
        new_owners = [('', '---Select an owner---')]
        for owner in owners:
            new_owners.append((owner, owner))
        self.fields["machine"].choices = new_machines
        self.fields["owner"].choices = new_owners


class MachineSearchForm(Form):
    """
    Class used in the filter section in Machines menu. Machines can be filtered only by name
    """
    machine = ChoiceField(choices=(), initial=('', '---Please select a machine---'), label="Machine", required=False,
                          widget=Select(attrs={"class": "custom-select mr-sm-2"}))

    def __init__(self, label_suffix, machines, parameters=None):
        """
        Search form constructor. Creates the form and dynamically updates the drop down lists using
        all the available DB entries.
        """
        super(MachineSearchForm, self).__init__(parameters, label_suffix)
        new_machines = [('', '---Select a machine---')]
        for machine in machines:
            new_machines.append((machine, machine))
        self.fields["machine"].choices = new_machines


class PidSearchForm(Form):
    """
    Form used to filter the available PIDs. The search field is used as free text for the description and page url
    and the issuer is a dropdown menu
    """
    search_for = CharField(
        required=False,
        widget=TextInput(attrs={"placeholder": "PID description, page url", "class": "form-control"}),
    )
    issuer = ChoiceField(choices=(), initial=('', '---Please select an issuer---'), label="Issuer", required=False,
                         widget=Select(attrs={"class": "custom-select mr-sm-2"}))

    def __init__(self, label_suffix, issuers, parameters=None):
        """
        Search form constructor. Creates the form and dynamically updates the drop down lists using
        all the available DB entries.
        """
        super(PidSearchForm, self).__init__(parameters, label_suffix)
        new_issuers = [('', '---Select an issuer---')]
        for issuer in issuers:
            new_issuers.append((issuer, issuer))
        self.fields["issuer"].choices = new_issuers


class UserSearchForm(Form):
    """
    Form to filter the users with drop down menus on usernames, names, last names and emails
    """
    usernames = ChoiceField(choices=(), initial=('', '---Please select a username---'), label="Username",
                            required=False,
                            widget=Select(attrs={"class": "custom-select mr-sm-2"}))
    first_names = ChoiceField(choices=(), initial=('', '---Please select a name---'), label="First Name",
                              required=False, widget=Select(attrs={"class": "custom-select mr-sm-2"}))
    last_names = ChoiceField(choices=(), initial=('', '---Please select a last name---'), label="LastName",
                             required=False, widget=Select(attrs={"class": "custom-select mr-sm-2"}))
    emails = ChoiceField(choices=(), initial=('', '---Please select an email---'), label="Email", required=False,
                         widget=Select(attrs={"class": "custom-select mr-sm-2"}))

    def __init__(self, label_suffix, usernames, first_names, last_names, emails, parameters=None):
        """
        Search form constructor. Creates the form and dynamically updates the drop down lists using
        all the available DB entries.
        """
        super(UserSearchForm, self).__init__(parameters, label_suffix)
        new_usernames = [('', '---Select a username---')]
        for username in usernames:
            new_usernames.append((username, username))
        new_first_names = [('', '---Select a name---')]
        for first_name in first_names:
            new_first_names.append((first_name, first_name))
        new_last_names = [('', '---Select a last name---')]
        for last_name in last_names:
            new_last_names.append((last_name, last_name))
        new_emails = [('', '---Select a email---')]
        for email in emails:
            new_emails.append((email, email))

        self.fields["usernames"].choices = new_usernames
        self.fields["first_names"].choices = new_first_names
        self.fields["last_names"].choices = new_last_names
        self.fields["emails"].choices = new_emails


class PidForm(Form):
    """
    Form to register a new PID. Only admins have access to register new data
    """
    pid_number = CharField(required=True, label="",
                           widget=TextInput(attrs={"placeholder": "PID number", "class": "form-control"}))
    pid_issuer = CharField(required=True, label="",
                           widget=TextInput(attrs={"placeholder": "PID issuer", "class": "form-control"}))
    page_url = CharField(required=True, label="",
                         widget=TextInput(attrs={"placeholder": "PID request page URL", "class": "form-control"}))
    description = CharField(required=True, label="",
                            widget=Textarea(attrs={"placeholder": "PID Description", "class": "form-control"}))


class MachineForm(Form):
    """
    Form to register a new machine in the DB
    """
    machine_name = CharField(required=True, label="",
                             widget=TextInput(attrs={"placeholder": "Machine Name", "class": "form-control"}))
    url = CharField(required=False, label="",
                    widget=TextInput(attrs={"placeholder": "Machine URL", "class": "form-control"}))


class AnnotationForm(Form):
    """
    Form to add new annotation in the pulse details menu
    """
    description = CharField(required=True, label="",
                            widget=Textarea(attrs={"placeholder": "New annotation", "class": "form-control"}))


class LoginForm(Form):
    """
    Class representing the login form in the first page
    """
    username = CharField(required=True, label="",
                         widget=TextInput(attrs={"placeholder": "Username", "class": "form-control"}))
    password = CharField(required=True, label="",
                         widget=PasswordInput(attrs={"placeholder": "Password", "class": "form-control"}))




class RegisterForm(Form):
    """
    Class representing the registration form used to create new users
    """
    first_name = CharField(required=True, label="",
                           widget=TextInput(attrs={"placeholder": "First Name", "class": "form-control"}))
    last_name = CharField(required=True, label="",
                          widget=TextInput(attrs={"placeholder": "Last Name", "class": "form-control"}))
    email = CharField(required=True, label="",
                      widget=TextInput(attrs={"placeholder": "Email", "class": "form-control"}))
    username = CharField(required=True, label="",
                         widget=TextInput(attrs={"placeholder": "Username", "class": "form-control"}))
    password = CharField(required=True, label="",
                         widget=PasswordInput(attrs={"placeholder": "Password", "class": "form-control"}))
    confirm_password = CharField(required=True, label="",
                                 widget=PasswordInput(attrs={"placeholder": "Confirm Password",
                                                             "class": "form-control"}))
    # role = ChoiceField(choices=(("admin", "Admin"), ("user", "User")), initial=("", "Any"), label="Role",
    #                    required=True,
    #                    widget=Select(attrs={"class": "custom-select mr-sm-2"}))


class ProfileForm(Form):
    """
    Form used in the profile menu to edit the signed in user's profile
    """
    first_name = CharField(required=False, label="",
                           widget=TextInput(attrs={"placeholder": "First Name", "class": "form-control"}))
    last_name = CharField(required=False, label="",
                          widget=TextInput(attrs={"placeholder": "Last Name", "class": "form-control"}))
    email = CharField(required=False, label="",
                      widget=TextInput(attrs={"placeholder": "Email", "class": "form-control"}))
    # current_password = CharField(required=False, label="",
    #                              widget=PasswordInput(attrs={"placeholder": "Current Password"}))
    # password = CharField(required=False, label="", widget=PasswordInput(attrs={"placeholder": "Password"}))
    # repeat_password = CharField(required=False, label="",
    #                             widget=PasswordInput(attrs={"placeholder": "Repeat Password"}))
