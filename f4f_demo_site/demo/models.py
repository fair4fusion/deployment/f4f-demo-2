import json
import logging

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver
from django.forms.models import model_to_dict
from django.utils import timezone
from django_mysql.models import JSONField

logger = logging.getLogger(__name__)


class AbstractClass(models.Model):
    """
    Abstract class containing columns appearing in all tables. Keeps information on who and when created or
    updated a DB entry. All following models extend this class
    """

    class Meta:
        abstract = True

    creation_date = models.DateTimeField()
    created_by = models.CharField(max_length=2000)
    modification_date = models.DateTimeField()
    modified_by = models.CharField(max_length=2000)

    def save(self, *args, **kwargs):
        if not self.id:
            self.creation_date = timezone.now()
            self.created_by = kwargs["username"] if "username" in kwargs.keys() else self.created_by
        self.modified_by = kwargs["username"] if "username" in kwargs.keys() else self.modified_by
        self.modification_date = timezone.now()
        super(AbstractClass, self).save()


class Experiment(AbstractClass):
    """
    This class represents a Fusion Machine (e.g. AUG, West etc). The Experiment class is identified by the
    short_name field which should be unique.
    """
    id = models.AutoField(primary_key=True, unique=True)
    short_name = models.CharField(max_length=32, blank=False, null=False)
    url = models.URLField()

    class Meta:
        indexes = [
            models.Index(fields=['short_name', 'url', ]),
        ]
        ordering = ['id']

    def __str__(self):
        """
        ToString method for Experiment model
        """
        return self.short_name



class Pulse(AbstractClass):
    """
    Pulse class contains all the information about the pulses, identified by the experiment, shot and number field
    combination. A pulse instance can be associated with multiple annotations, concatenated in the cannots field.
    """
    # All time-series data should be provided in the form:
    # { "time": [t1, t2,...], "data": [d1, d2, ...] }
    id = models.AutoField(primary_key=True, unique=True)
    shot = models.IntegerField(blank=False, null=False)
    number = models.IntegerField(blank=False, null=False, default=0)
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE, null=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    s_code_commit = models.CharField(max_length=128, null=True, blank=True)
    s_code_name = models.CharField(max_length=128, null=True, blank=True)
    s_code_repository = models.CharField(max_length=128, null=True, blank=True)

    # summary/global_quantities/b0/value  [AUG]
    s_globalQuantities_b0_value = JSONField(null=True, blank=True)
    # summary/global_quantities/b0/value_error_upper  [AUG]
    s_globalQuantities_b0_valueErrorUpper = JSONField(null=True, blank=True)
    # summary/global_quantities/b0/source  [AUG]
    s_globalQuantities_b0_source = models.CharField((""), null=True,
                                                    blank=True,
                                                    max_length=50)

    # summary/heating_current_drive/power_nbi/value  [AUG]
    s_heatingCurrentDrive_powerNbi_value = JSONField(null=True, blank=True)
    # summary/heating_current_drive/power_nbi/value_error_upper  [AUG]
    s_heatingCurrentDrive_powerNbi_valueErrorUpper = JSONField(null=True,
                                                               blank=True)
    # summary/heating_current_drive/power_nbi/source  [AUG]
    s_heatingCurrentDrive_powerNbi_source = models.CharField((""),
                                                             max_length=50,
                                                             blank=True,
                                                             null=True)
    # summary/global_quantities/r0/value (single value)  [AUG]
    s_globalQuantities_r0_value = models.FloatField(null=True, blank=True)
    # summary/global_quantities/r0/value_error_upper  [AUG]
    s_globalQuantities_r0_valueErrorUpper = models.FloatField(null=True,
                                                              blank=True)

    # summary/fusion/neutron_fluxes/total/source
    s_fusion_neutronFluxes_total_source = models.CharField(max_length=128,
                                                           null=True,
                                                           blank=True)
    # summary/fusion/neutron_fluxes/total/value
    s_fusion_neutronFluxes_total_value = JSONField(null=True, blank=True)

    # summary/global_quantities/beta_pol/value
    s_globalQuantities_betapol_value = JSONField(null=True, blank=True)
    # summary/global_quantities/beta_tor/value
    s_globalQuantities_betaTor_value = JSONField(null=True, blank=True)
    # summary/global_quantities/current_ohm/value
    s_globalQuantities_currentOhm_value = JSONField(null=True, blank=True)
    # summary/global_quantities/energy_b_field_pol/value
    s_globalQuantities_energyBFieldPol_value = JSONField(null=True, blank=True)
    # summary/global_quantities/energy_diamagnetic/value
    s_globalQuantities_energyDiamagnetic_value = JSONField(null=True,
                                                           blank=True)
    # summary/global_quantities/energy_total/value
    s_globalQuantities_energyTotal_value = JSONField(null=True, blank=True)

    # summary/time_width  [AUG]
    s_timeWidth = JSONField(null=True, blank=True)
    # summary/time  [AUG]
    s_time = JSONField(null=True, blank=True)

    # summary/global_quantities/ip/value  [AUG]
    s_globalQuantities_ip_value = JSONField(null=True, blank=True)
    # summary/global_quantities/ip/value_error_upper  [AUG]
    s_globalQuantities_ip_valueErrorUpper = JSONField(null=True, blank=True)
    # summary/global_quantities/ip/source  [AUG]
    s_globalQuantities_ip_source = models.CharField((''), null=True,
                                                    blank=True,
                                                    max_length=50)

    # summary/global_quantities/li/value
    s_globalQuantities_li_value = JSONField(null=True, blank=True)

    # summary/global_quantities/tau_energy/value
    s_globalQuantities_tauEnergy_value = JSONField(null=True, blank=True)
    # summary/global_quantities/volume/value
    s_globalQuantities_volume_value = JSONField(null=True, blank=True)
    # summary/global_quantities/v_loop/value
    s_globalQuantities_vLoop_value = JSONField(null=True, blank=True)
    # summary/ids_properties/creation_date
    s_idsProperties_creationDate = models.DateTimeField()
    # summary/ids_properties/homogeneous_time
    s_idsProperties_homogeneousTime = models.TimeField()
    # summary/line_average/zeff/source
    s_lineAverage_zeff_source = models.TextField(null=True, blank=True)
    # summary/volume_average/n_e/source
    s_volumeAverage_nE_source = models.CharField(max_length=100, null=True,
                                                 blank=True)
    # summary/volume_average/n_e/value
    s_volumeAverage_nE_value = JSONField(null=True, blank=True)

    cannots = models.TextField(null=False, blank=True)

    def __str__(self):
        """
        ToString method for Pulse model
        """
        return ('(' + str(self.shot) + '/' + str(self.number) + ') ' +
                str(self.s_idsProperties_creationDate))

    class Meta:
        unique_together = ("shot", "number", "experiment")
        indexes = [
            models.Index(fields=['shot', 'number', ]),
        ]
        ordering = ['id']

    def get_annotations(self):
        """
        Gets the list of annotations associated with the specific pulse
        """
        return Annotation.objects.filter(pulse=self)

    def get_pids(self):
        """
        Gets the list of PIDs associated with the specific pulse
        """
        return Pid.objects.filter(pulses__id=self.id)


class Annotation(AbstractClass):
    """
    This class represents an annotation / comment on a pulse.
    """
    id = models.AutoField(primary_key=True, unique=True)
    user = models.ForeignKey(User, default=1, on_delete=models.DO_NOTHING)
    text = models.TextField(null=False, blank=False)
    time = models.DateTimeField(auto_now=True)
    pulse = models.ForeignKey(Pulse, on_delete=models.DO_NOTHING, null=True)

    def __str__(self):
        """
        toString method for the Annotation model
        """
        return '(' + str(self.time) + ') ' + str(self.user) + ': ' + self.text

    class Meta:
        indexes = [
            models.Index(fields=['time', ]),
        ]
        ordering = ['id']


class Pid(AbstractClass):
    """
    This class represents a Pid assigned to pulse(s).
    """

    class Meta:
        ordering = ['id']

    id = models.AutoField(primary_key=True, unique=True)
    pulses = models.ManyToManyField(Pulse)
    pid_number = models.CharField(max_length=255, unique=True, null=False, blank=False)
    issuer = models.CharField(max_length=1000)
    description = models.TextField()
    page_url = models.TextField(null=False, blank=False)
    search_description = models.TextField(null=False, blank=True)

    def __str__(self):
        """
        Overrides the __str__ object method to show all the object fields & values as strings.

        Returns
            str: string containing all the class fields and their values
        """
        obj = model_to_dict(self)
        obj["creation_date"] = str(self.creation_date)
        obj["modification_date"] = str(self.modification_date)
        return json.dumps(obj)


class Search(models.Lookup):
    """
    This class is used for supporting search:Pulse.objects.filter(cannots__search='query')
    """
    lookup_name = 'search'

    def as_mysql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return 'MATCH (%s) AGAINST (%s IN BOOLEAN MODE)' % (lhs, rhs), params


models.CharField.register_lookup(Search)
models.TextField.register_lookup(Search)


@receiver(post_save, sender=Annotation)
def save_profile(sender, instance, **kwargs):
    """
    This function is called right after an annotation insertion in the DB and updates the cannots field of the
    relevant pulse db entry.

    Args
        | sender: the request sender
        | instance: the annotation instance that is just persisted in the DB
        | kwargs: additional arguments
    """
    logger.debug("New annotation: {}".format(instance))
    pulse = Pulse.objects.get(pk=instance.pulse.id)
    pulse.cannots = ' '.join([x.text + ' - ' + str(x.user) + ' | '
                              for x in Annotation.objects.filter(pulse=pulse)])
    pids = Pid.objects.filter(pulses=pulse)
    if pids:
        pulse.cannots = pulse.cannots + "$$"
        for pid in pids:
            pulse.cannots = pulse.cannots + pid.description + " - " + pid.page_url + ' | '
        logger.debug("Pulse with ID {} has new cannots {}".format(pulse.id, pulse.cannots))
    pulse.save()


@receiver(post_delete, sender=Annotation)
def on_annotation_delete(sender, instance, **kwargs):
    """
    This function updates the concatenated text in a pulse when an annotation is deleted from the DB

    Args
        | sender: the request sender
        | instance: the annotation instance that is just persisted in the DB
        | kwargs: additional arguments
    """
    logger.debug("Deleted annotation: {}".format(instance))
    pulse = Pulse.objects.get(pk=instance.pulse.id)
    pulse.cannots = ' '.join([x.text + ' - ' + str(x.user) + ' | '
                              for x in Annotation.objects.filter(pulse=pulse)])
    pids = Pid.objects.filter(pulses=pulse)
    if pids:
        pulse.cannots = pulse.cannots + "$$"
        for pid in pids:
            pulse.cannots = pulse.cannots + pid.description + " - " + pid.page_url + ' | '
        logger.debug("Pulse with ID {} updated cannots: {}".format(pulse.id, pulse.cannots))
    pulse.save()


@receiver(pre_save, sender=Pid)
def pre_pid_save(sender, instance, **kwargs):
    """
        This function is called right before a PID insertion in the DB and updates the search_description field of the
        relevant pulse db entry.

        Args
            | sender: the request sender
            | instance: the annotation instance that is just persisted in the DB
            | kwargs: additional arguments
    """
    instance.search_description = instance.description + " #### " + instance.page_url
    logger.debug("PID instance updated with search descr= {}".format(instance.search_description))


@receiver(post_save, sender=Pid)
def on_pid_save(sender, instance, **kwargs):
    """
        This function is called right after a PID insertion in the DB and updates the cannots field of the
        relevant pulse db entry.

        Args
            | sender: the request sender
            | instance: the annotation instance that is just persisted in the DB
            | kwargs: additional arguments
    """
    logger.debug("New PID saved in the DB: {}".format(instance))
    pulses = instance.pulses
    if pulses and type(pulses) == list:
        for pulse in pulses:
            pulse.cannots = ' '.join([x.text + ' - ' + str(x.user) + ' | '
                                      for x in Annotation.objects.filter(pulse=pulse)])
            pids = Pid.objects.filter(pulses=pulse)
            if pids:
                pulse.cannots = pulse.cannots + "$$"
                for pid in pids:
                    pulse.cannots = pulse.cannots + pid.description + " - " + pid.page_url + ' | '
            logger.debug("Pulse with ID {} was updated with cannots {}".format(pulse.id, pulse.cannots))
            pulse.save()


@receiver(post_delete, sender=Pid)
def on_pid_delete(sender, instance, **kwargs):
    """
        This function is called right after a PID is deleted from the DB and updates the cannots field of the
        relevant pulse db entry.

        Args
            | sender: the request sender
            | instance: the annotation instance that is just persisted in the DB
            | kwargs: additional arguments
    """
    logger.debug("PID delete: {}".format(instance))
    pulses = instance.pulses
    if pulses and type(pulses) == list:
        for pulse in pulses:
            pulse.cannots = ' '.join([x.text + ' - ' + str(x.user) + ' | '
                                      for x in Annotation.objects.filter(pulse=pulse)])
            pids = Pid.objects.filter(pulses=pulse)
            if pids:
                pulse.cannots = pulse.cannots + "$$"
                for pid in pids:
                    pulse.cannots = pulse.cannots + pid.description + " - " + pid.page_url + ' | '
                logger.debug("Pulse with id {} was updated with cannots {}".format(pulse.id, pulse.cannots))
            pulse.save()
