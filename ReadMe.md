# f4f-demo-2
FAIR4Fusion Demonstration 2

This is the main repository for the FAIR4Fusion alternative demonstrator. 

## Execution instructions

1. ```bash
    pip install --user -r requirements.txt
    ```

2. Go to f4f_demo_site/f4f_demo_site folder and copy the example_properties.json in a new file named properties.json
    a. Update the DATABASES property with your local configuration
    b. Update the plots section with what you need
    c. Change the populate_db parameter if you want to add new data in the DB

3. Execute the following:
    ```bash
        cd f4f_demo_site
        python manage.py makemigrations
        python manage.py migrate
        python manage.py migrate --run-syncdb
        python manage.py createsuperuser
        python manage.py collectstatic
    ```
4. Go to the f4f_demo_site/db folder and execute the SQL command in the add_fulltext_index.sql

## Sphinx documentation

sphinx-apidoc -f --ext-autodoc --ext-viewcode --ext-coverage -o source/ ../

## Kubernetes deployment

* kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=demo --docker-password=uSJG83ELDFsRaS7q132b

## Rabbitmq user

```bash
rabbitmqctl add_user f4f f4f
rabbitmqctl set_user_tags f4f administrator
rabbitmqctl set_permissions -p / f4f ".*" ".*" ".*"
```


## Run celery locally

```bash
 celery -A f4f_demo_site worker --broker="amqp://f4f:f4f@localhost:5672//"  --loglevel=debug
```

## Keycloak

Username: keycloak

To retrieve the initial user password run:

```bash
    kubectl get secret --namespace default keycloak-http -o jsonpath="{.data.password}" | base64 --decode; echo
```
