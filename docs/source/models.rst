Django Models for F4F Demonstrator
===================================

.. toctree::
        serializers

.. automodule:: demo.models
   :members: