UI views for F4F Demonstrator
================================

.. toctree::
        futils
        forms

.. automodule:: demo_ui.views
   :members:
   :undoc-members:
